using System;

public class GameController : Singleton<GameController>
{
    public void ShowInter(Action mustRunCallback)
    {
        mustRunCallback?.Invoke();
    }
}