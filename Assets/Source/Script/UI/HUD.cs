using UnityEngine;

public abstract class Hud : MonoBehaviour
{
    public HudType type;
    public abstract void Setup(object obj);

    public virtual void Show()
    {
        gameObject.SetActive(true);
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }

    public virtual void HideUI()
    {
        var ui = GetComponentsInChildren<IHideable>(true);
        foreach (var obj in ui) obj.HideUI();
    }
    
    public virtual void ShowUI()
    {
        var ui = GetComponentsInChildren<IHideable>(true);
        foreach (var obj in ui) obj.ShowUI();
    }
}

public interface IHideable
{
    void HideUI();
    void ShowUI();
}

public interface IRegisterEvent
{
    void RegisterEvent();
    
    void UnRegisterEvent();
}