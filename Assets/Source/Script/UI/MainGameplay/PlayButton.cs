using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class PlayButton : MonoBehaviour
{
    [SerializeField] VideoPlayer video;
    
    public void PlayVideo()
    {
        video.Play();
    }

    public void PauseVideo()
    {
        video.Pause();
    }

    public void ReplayVideo()
    {
        video.Stop();
        video.Play();
    }

    public void OnClick()
    {
        if (video.isPlaying) PauseVideo();
        else PlayVideo();
    }
}
