using System;
using UnityEngine;
using UnityEngine.Video;

public class VideoPlayUI : Hud
{
    [SerializeField] private VideoPlayer video;

    private void Start()
    {
        foreach (var obj in GetComponentsInChildren<IRegisterEvent>(true)) obj.RegisterEvent();
    }

    private void OnDestroy()
    {
        foreach (var obj in GetComponentsInChildren<IRegisterEvent>(true)) obj.UnRegisterEvent();
    }

    public override void Setup(object obj)
    {
        
    }
}