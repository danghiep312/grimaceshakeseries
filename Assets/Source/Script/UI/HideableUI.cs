using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;


public class HideableUI : MonoBehaviour, IHideable
{
    private RectTransform _rect;
    public Vector2 originalPos;
    

    public void HideUI()
    {
        _rect??=GetComponent<RectTransform>();
        _rect.DOAnchorPosY(-originalPos.y, 0.5f).SetEase(Ease.InBack);
    }

    public void ShowUI()
    {
        _rect??=GetComponent<RectTransform>();
        _rect.DOAnchorPosY(originalPos.y, 0.5f).SetEase(Ease.OutBack);
    }


    [Button]
    public void GetPos()
    {
        originalPos = GetComponent<RectTransform>().anchoredPosition;
    }
}
