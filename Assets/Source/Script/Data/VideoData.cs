using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.Video;
using Random = UnityEngine.Random;


[CreateAssetMenu(fileName = "VideoData", menuName = "Data/VideoData")]
public class VideoData : ScriptableObject
{
    public int id;
    public string videoName;
    public VideoClip video;
    private Info _info;

    public VideoData()
    {
        _info = new Info();
    }

    public Info Info => _info;
}

[Serializable]
public class Info
{
    public float Views;
    public float Likes;
    public float Shares;

    public Info()
    {
        Views = (float) Math.Round(Random.Range(15f, 40f));
        Likes = (float) Math.Round(Random.Range(500f, 800f));
        Likes = (float) Math.Round(Random.Range(500f, 800f));
    }
}